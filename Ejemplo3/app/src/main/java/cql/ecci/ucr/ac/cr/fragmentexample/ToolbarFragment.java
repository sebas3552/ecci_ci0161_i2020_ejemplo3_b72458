package cql.ecci.ucr.ac.cr.fragmentexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

public class ToolbarFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    private static int seekvalue = 10;
    private static EditText edittext;

    ToolbarListener activityCallBack;

    public interface ToolbarListener{
        public void onButtonClick(int fontsize, String text);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try{
            activityCallBack = (ToolbarListener) context;
        }catch(ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement ToolbarListener.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.toolbar_fragment, container, false);

        edittext = (EditText) view.findViewById(R.id.editText1);

        final SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar1);

        seekBar.setOnSeekBarChangeListener(this);

        final Button button = (Button) view.findViewById(R.id.button1);

        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                buttonClicked(v);
            }
        });

        return view;
    }

    public void buttonClicked(View view){
        activityCallBack.onButtonClick(seekvalue, edittext.getText().toString());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
        seekvalue = progress;
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0){

    }

    @Override
    public void onStopTrackingTouch(SeekBar arg0){

    }
}
