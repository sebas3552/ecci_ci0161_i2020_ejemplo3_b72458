package cql.ecci.ucr.ac.cr.fragmentexample;

import android.os.Bundle;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

public class FragmentExampleActivity extends FragmentActivity implements ToolbarFragment.ToolbarListener{

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_fragment_example);
    }

    @Override
    public void onButtonClick(int fontsize, String text){
        TextFragment textFragment = (TextFragment) getSupportFragmentManager().findFragmentById(R.id.text_fragment);
        textFragment.changeTextProperties(fontsize, text);
    }
}
